from django import forms
from .models import Productos
from django.contrib.auth.forms import AuthenticationForm


class ProductoForm (forms.ModelForm):
	class Meta:
		model = Productos
		fields = [
			'Codigo', 
			'Nombre',
			'Valor_iva',
			'Valor_total',
		]

		widgets = {
			'Codigo': forms.NumberInput(
				attrs = {
					'class':'form-control',
					'placeholder': 'Codigo de Producto'
				}
			),
			'Nombre': forms.TextInput(
				attrs = {
					'class':'form-control',
					'placeholder': 'Nombre de Producto'
				}
			),
			'Valor_iva': forms.NumberInput(
				attrs = {
					'class':'form-control',
					'placeholder': 'Valor de Iva'
				}
			),
			'Valor_total': forms.NumberInput(
				attrs = {
					'class':'form-control',
					'placeholder': 'Valor Total'
				}
			),
	}


class FormularioLogin(AuthenticationForm):
    def __init__(self, *args, **kwargs):
        super(FormularioLogin, self).__init__(*args,**kwargs)
        self.fields['username'].widget.attrs['class'] = 'form-control'
        
        self.fields['password'].widget.attrs['class'] = 'form-control'
        
    