from django.shortcuts import render,redirect
from django.forms import ModelForm
from .models import Productos
from .forms import ProductoForm
from django.views.generic import View,TemplateView,ListView,UpdateView,CreateView,DeleteView
from django.urls import reverse_lazy
from django.views.generic.edit import FormView
from django.contrib.auth import login,logout
from django.http import HttpResponseRedirect
from .forms import FormularioLogin
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.utils.decorators import method_decorator

# Create your views here.	

def Index(request):
    return render(request,'Index.html') 

def stock (request):
	return render(request,'stock.html')

def modificar (request):
	return render(request,'Modificar.html')


class Listproducto(ListView):
	model= Productos
	template_name='stock.html'

class Updateproducto (UpdateView):
	model= Productos
	form_class = ProductoForm
	template_name='Modificar.html'
	success_url= reverse_lazy('consulta')

class Createproducto(CreateView):
    model=Productos
    form_class = ProductoForm
    template_name ="Index.html"
    success_url = reverse_lazy('consulta')
    
class Deleteproducto(DeleteView):
	model= Productos
	success_url = reverse_lazy('consulta')
    

class Login(FormView):
    template_name = 'login.html'
    form_class = FormularioLogin
    success_url = reverse_lazy('crear')

    @method_decorator(csrf_protect)
    @method_decorator(never_cache)
    def dispatch(self,request,*args,**kwargs):
        if request.user.is_authenticated:
            return HttpResponseRedirect(self.get_success_url())
        else:
            return super(Login,self).dispatch(request,*args,**kwargs)

    def form_valid(self,form):
        login(self.request,form.get_user())
        return super(Login,self).form_valid(form)

def logoutUsuario(request):
    logout(request)
    return HttpResponseRedirect('/accounts/login/')

