from django.urls import path , include
from .views import Index,stock,Listproducto,Updateproducto,modificar,Createproducto,Deleteproducto,Login,logoutUsuario
from django.contrib import admin
from django.contrib.auth import login,logout
from django.contrib.auth.decorators import login_required

urlpatterns = [
    #path('',Index,name="Index"),
    path('',login_required(Createproducto.as_view()),name="crear"),
    path('consulta',login_required(Listproducto.as_view()),name="consulta"),
    path('modificar/<int:pk>',login_required(Updateproducto.as_view()),name="modificar"),
    path('eliminar/<int:pk>',login_required(Deleteproducto.as_view()),name="eliminar"),
    path('accounts/login/',Login.as_view(), name = 'login'),
    path('logout/',login_required(logoutUsuario),name = 'logout'),
    path('accounts/',include('allauth.urls')),
]