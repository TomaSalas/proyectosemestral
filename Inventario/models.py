from django.db import models
from django.urls import reverse

class Productos(models.Model):
    Id = models.AutoField(primary_key= True)
    Codigo = models.IntegerField(unique=True ,error_messages={'required': 'Codigo Ya Existente'})
    Nombre = models.CharField(max_length=200)
    Valor_iva = models.IntegerField()
    Valor_total = models.IntegerField()

    def __str__(self):
        return self.Nombre

    def get_absolute_url(self):
        return reverse('Productos_edit', kwargs={'pk': self.pk})
        
